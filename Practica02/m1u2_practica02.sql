USE M1U2_Practica02;

/*****************************************************************************
*
*   M�dulo 1 - Unidad 2 - Pr�ctica 02
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/
-- 1 -
SELECT 
  COUNT(*)
FROM
  Ciudad c;

-- 2 -
SELECT c.nombre
FROM Ciudad c
WHERE c.poblaci�n > (
  SELECT AVG(poblaci�n) 
  FROM Ciudad
);

-- 3 -
SELECT c.nombre
FROM Ciudad c
WHERE c.poblaci�n < (
  SELECT AVG(poblaci�n) 
  FROM Ciudad
);

-- 4 -
SELECT *
FROM Ciudad c
WHERE c.poblaci�n = (
  SELECT MAX(poblaci�n) 
  FROM Ciudad
);

-- 5 -
SELECT *
FROM Ciudad c
WHERE c.poblaci�n = (
  SELECT MIN(poblaci�n) 
  FROM Ciudad
);

-- 6 -
SELECT COUNT(*)
FROM Ciudad c
WHERE c.poblaci�n > (
  SELECT AVG(poblaci�n) 
  FROM Ciudad
);


-- 7 -
SELECT p.ciudad, COUNT(*)
FROM Persona p
GROUP BY p.ciudad;


-- 8 -
SELECT t.compa�ia, COUNT(t.compa�ia)
FROM Trabaja t
GROUP BY t.compa�ia;

-- 9 - Compa�ia que tiene m�s trabajadores
SELECT t.compa�ia
FROM Trabaja t
GROUP BY t.compa�ia
HAVING COUNT(t.compa�ia) = (
  SELECT MAX(censo.trabajadores)
  FROM ( 
    SELECT t.compa�ia, COUNT(t.compa�ia) AS trabajadores
    FROM Trabaja t
    GROUP BY t.compa�ia
  ) AS censo
);

SELECT t.compa�ia
FROM 
( 
  SELECT t.compa�ia, COUNT(t.compa�ia) AS trabajadores
  FROM Trabaja t
  GROUP BY t.compa�ia
) AS censo
INNER JOIN
(
  SELECT MAX(censo.trabajadores) AS maximo
  FROM 
  ( SELECT t.compa�ia, COUNT(t.compa�ia) AS trabajadores
    FROM Trabaja t
    GROUP BY t.compa�ia
  ) AS censo
) AS nempleados
ON censo.trabajadores = nempleados.maximo;


-- 10 - SALARIO MEDIO POR COMPA�IA
SELECT AVG(salario)
FROM Trabaja t
GROUP BY t.compa�ia;


-- 11 - Personas y ciudad donde viven
SELECT p.nombre, c.poblaci�n
FROM 
  persona p
INNER JOIN
  ciudad c
ON p.ciudad = c.nombre;


-- 12 Listar el nombre de las personas, la calle donde vive y la poblaci�n de la ciudad donde vive   
SELECT p.nombre, p.calle, c.poblaci�n
FROM 
  persona p
INNER JOIN
  ciudad c
ON p.ciudad = c.nombre;

  
-- 13. Listar el nombre de las personas, la ciudad donde vive y la ciudad donde est� la compa��a para la que trabaja.
SELECT p.nombre, p.ciudad AS viveEn, c.ciudad AS trabajaEn
FROM 
  persona p 
INNER JOIN 
  trabaja t 
ON p.nombre = t.persona
INNER JOIN 
  compa�ia c
ON t.compa�ia = c.nombre;


-- 14. Realizar el algebra relacional y explicar la siguiente consulta 
-- FILTRO(nombre)SELECCION(p.ciudad=c.ciudad)(persona (X) trabaja (X) compa�ia)
-- Listado alfabetizado de nombres de las personas que viven en la misma ciudad en la que trabajan
SELECT p.nombre
FROM 
  persona p 
INNER JOIN 
  trabaja t 
ON p.nombre = t.persona
INNER JOIN 
  compa�ia c
ON t.compa�ia = c.nombre
WHERE p.ciudad = c.ciudad
ORDER BY p.nombre ASC;


-- 15. Listarme el nombre de la persona y el nombre de su supervisor 
SELECT s.persona, s.supervisor
FROM 
  persona trabajador
INNER JOIN
  supervisa s
ON trabajador.nombre = s.persona
INNER JOIN
  persona supervisor
ON s.supervisor = supervisor.nombre;


-- 16. Listarme el nombre de la persona, el nombre de su supervisor y las ciudades donde residen cada una de ellos 
SELECT 
  tra.nombre AS trabajador_nombre, 
  tra.ciudad AS trabajador_ciudad, 
  supe.nombre AS supervisor_nombre, 
  supe.ciudad AS supervisor_ciudad
FROM persona tra
INNER JOIN supervisa s ON tra.nombre = s.persona
INNER JOIN persona supe ON s.supervisor = supe.nombre;


-- 17. Indicarme el n�mero de ciudades distintas que hay en la tabla compa��a
SELECT COUNT(*) 
FROM 
( SELECT DISTINCT c.ciudad
  FROM compa�ia c
) AS ciudadesDistintas;

-- 18. Indicarme el n�mero de ciudades distintas que hay en la tabla personas
SELECT COUNT(*) 
FROM 
( SELECT DISTINCT p.ciudad
  FROM persona p
) AS ciudadesDistintas;

-- 19. Indicarme el nombre de las personas que trabajan para FAGOR
SELECT t.persona
FROM trabaja t
WHERE t.compa�ia = 'FAGOR';


-- 20. Indicarme el nombre de las personas que no trabajan para FAGOR
-- Trabajadores que no trabajan para FAGOR
SELECT t.persona
FROM trabaja t
WHERE t.compa�ia != 'FAGOR';

SELECT p.nombre
FROM
  persona p
INNER JOIN
  trabaja t
ON p.nombre = t.persona
WHERE t.compa�ia != 'FAGOR';

SELECT p.nombre
FROM 
  persona p
LEFT JOIN
  trabaja t
ON p.nombre = t.persona
WHERE t.persona IS NULL OR (t.compa�ia != 'FAGOR');


-- 21. Indicarme el n�mero de personas que trabajan para INDRA
SELECT COUNT(*)
FROM trabaja t
WHERE t.compa�ia = 'INDRA';

-- 22. Indicarme el nombre de las personas que trabajan para FAGOR o para INDRA
SELECT t.persona
FROM trabaja t
WHERE t.compa�ia = 'FAGOR' OR t.compa�ia = 'INDRA';

( SELECT persona FROM trabaja t WHERE t.compa�ia = 'FAGOR' )
UNION
( SELECT persona FROM trabaja t WHERE t.compa�ia = 'INDRA' );


-- 23. Listar la poblaci�n donde vive cada persona, sus salario, su nombre y la compa��a para la que trabaja. Ordenar la salida por nombre de la persona y por salario de forma descendente. 
SELECT p.ciudad, t.salario, p.nombre, t.compa�ia
FROM
  persona p
INNER JOIN
  trabaja t
ON p.nombre = t.persona
ORDER BY p.nombre DESC, t.salario DESC;

 