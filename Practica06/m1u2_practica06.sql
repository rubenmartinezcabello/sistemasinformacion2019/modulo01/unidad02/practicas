/*****************************************************************************
*
*   M�dulo 1 - Unidad 2 - Pr�ctica 06
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/
CREATE DATABASE IF NOT EXISTS `m1u2_practica06` 
  DEFAULT CHARACTER SET utf8mb4 
  DEFAULT COLLATE utf8mb4_bin
  CHARACTER SET=utf8mb4 
  COLLATE=utf8mb4_spanish_ci;

USE `m1u2_practica06`;

DROP TABLE articulo;
DROP TABLE revista;
DROP TABLE tema;
DROP TABLE autor;


-- Realizar el script de creaci�n de las tablas y de las restricciones. 
CREATE OR REPLACE TABLE autor (
  dni     char(9),
  nombre  varchar(127),
  universidad varchar (50),
  PRIMARY KEY (dni)
) ENGINE=INNODB;

CREATE OR REPLACE TABLE tema (
  codtema int AUTO_INCREMENT,
  descripcion varchar(255),
  PRIMARY KEY (codtema)
) ENGINE=INNODB;

CREATE OR REPLACE TABLE revista (
  referencia int AUTO_INCREMENT,
  titulo_rev varchar(50),
  editorial varchar(255),
  PRIMARY KEY (referencia)
) ENGINE=INNODB;
          
CREATE OR REPLACE TABLE articulo (
    referencia int,
    codtema int,
    dni char(9),
    titulo_art varchar(50),
    anno year,
    volumen int,
    numero int,
    paginas int,
    PRIMARY KEY (referencia, codtema,dni)
  ) ENGINE=innodb;

-- Restricciones

ALTER TABLE articulo
  DROP CONSTRAINT IF EXISTS fk_referenciaRevista;
ALTER TABLE articulo
  DROP CONSTRAINT IF EXISTS fk_codtemaTema;
ALTER TABLE articulo
  DROP CONSTRAINT IF EXISTS fk_dniAutor;

ALTER TABLE articulo
  ADD CONSTRAINT fk_referenciaRevista
    FOREIGN KEY (referencia)
      REFERENCES revista (referencia);
ALTER TABLE articulo
  ADD CONSTRAINT fk_codtemaTema
    FOREIGN KEY (codtema)
      REFERENCES tema (codtema);
ALTER TABLE articulo
  ADD CONSTRAINT fk_dniAutor
    FOREIGN KEY (dni)
      REFERENCES autor (dni);


-- Introducir datos
INSERT INTO autor (dni, nombre, universidad) VALUES 
  ("66666666K", "Arturo P�rez-Reverte Guti�rrez", "Universidad Complutense de Madrid"), 
  ("30000000L", "Neil deGrasse Tyson", "Princeton University"), 
  ("51331934M", "Joanne K. Rowling", "University of Exeter");

INSERT INTO tema (descripcion)
  VALUES ('Misterio'),('Ciencias'),('Historia'),('Novela'), ('SQL'),('Medicina');

INSERT INTO revista (titulo_rev, editorial)
  VALUES 
    ('Astrophysical Journal Letters', 'American Astrophysical Society Publishing'),
    ('El Pais Semanal', 'Grupo Prisa'),
    ('Nature', 'Springer-Nature Research'),
    ('Muy Historia', 'Zinet Media Global');

INSERT INTO articulo (dni, codtema, referencia, titulo_art, anno, volumen, numero, paginas)
  VALUES 
    ('66666666K', 4, 2, 'Alatriste', 1990, 2008, 3, 198),
    ('66666666K', 3, 4, 'Guerra en Flandes', 1993, 2009, 3, 1),
    ('30000000L', 2, 1, 'New tidings about galactic expansion rate', 1992, 153, 3, 2),
    ('30000000L', 3, 4, 'Modern Black History', 1991, 714, 3, 1),
    ('51331934M', 1, 2, 'Cormoran Strike', 2008, 3, 3, 214),
    ('51331934M', 4, 3, 'Fantastic beasts and where to find them', 2005, 20, 3, 1),
    ('30000000L', 5, 2, 'Database Techniquess for Astronomic Data', 1991, 174, 3, 4),
    ('51331934M', 6, 3, 'Clasical British Medicinal Plants', 2008, 22, 3, 5),
    ('30000000L', 5, 3, 'Best observatories in the world and data access', 1992, 169, 3, 6);
; 

/*
SELECT * 
  FROM 
    articulo a 
    INNER JOIN autor u USING(dni) 
    INNER JOIN tema t USING(codtema) 
    INNER JOIN revista r USING (referencia)
;
*/
-- Consultas

-- 1 - Titulo del art�culo sobre "bases de datos" publicados en 1990
SELECT a.titulo_art 
  FROM 
    articulo a 
    INNER JOIN tema t USING(codtema) 
  WHERE
    a.anno = 1990
    AND 
    t.descripcion = "BASE DE DATOS"
;

-- 2 - Titulo de revista que tiene art�culos de todos los temas
SELECT DISTINCT a.referencia, a.codtema FROM articulo a;
SELECT DISTINCT a.codtema FROM articulo a;

SELECT referencia, dividendo.codtema, COUNT(dividendo.codtema)
  FROM 
    (SELECT DISTINCT a.referencia, a.codtema FROM articulo a) dividendo
  GROUP BY dividendo.referencia
  HAVING COUNT(dividendo.codtema) = (SELECT COUNT(DISTINCT a.codtema) FROM articulo a);
  ;

-- 3.0 - Titulos de revistas que no tengan art�culos sobre medicina
SELECT DISTINCT referencia FROM revista r;
SELECT DISTINCT referencia FROM articulo a INNER JOIN tema USING (codtema) WHERE descripcion = "Medicina";
-- 3.1 not in
SELECT titulo_rev 
  FROM 
    revista 
  WHERE 
    referencia IN (SELECT DISTINCT referencia FROM revista r) 
  AND  
    referencia NOT IN (SELECT DISTINCT referencia FROM articulo a INNER JOIN tema USING (codtema) WHERE descripcion = "Medicina")
;
-- 3.2 left join
SELECT titulo_rev 
  FROM 
    revista r
    INNER JOIN 
    (
      SELECT minuendo.referencia
        FROM
         (SELECT DISTINCT referencia FROM revista r) minuendo
        LEFT JOIN
         (SELECT DISTINCT referencia FROM articulo a INNER JOIN tema USING (codtema) WHERE descripcion = "Medicina") sustraendo
        ON minuendo.referencia = sustraendo.referencia
      WHERE sustraendo.referencia IS NULL
    ) diferencia
    ON r.referencia = diferencia.referencia
;

-- 4.1 - Autor que haya escrito sobre sql en el a�o 1991 y 1992
/*
SELECT dni FROM articulo INNER JOIN tema USING (codtema) WHERE descripcion = 'SQL' AND anno = 1991;
SELECT dni FROM articulo INNER JOIN tema USING (codtema) WHERE descripcion = 'SQL' AND anno = 1992;

 
(SELECT dni FROM articulo INNER JOIN tema USING (codtema) WHERE descripcion = 'SQL' AND anno = 1991)
intersect
(SELECT dni FROM articulo INNER JOIN tema USING (codtema) WHERE descripcion = 'SQL' AND anno = 1992)
;
*/
-- 4.2 - Autor que haya escrito sobre sql en el a�o 1991 y 1992
SELECT * 
  FROM
    (SELECT dni FROM articulo INNER JOIN tema USING (codtema) WHERE descripcion = 'SQL' AND anno = 1991) a
    INNER JOIN 
    (SELECT dni FROM articulo INNER JOIN tema USING (codtema) WHERE descripcion = 'SQL' AND anno = 1992) b
    USING (dni);

SELECT autor.nombre 
  FROM
    autor
    INNER JOIN 
    (
      SELECT * FROM 
        (SELECT dni FROM articulo INNER JOIN tema USING (codtema) WHERE descripcion = 'SQL' AND anno = 1991) a
        INNER JOIN 
        (SELECT dni FROM articulo INNER JOIN tema USING (codtema) WHERE descripcion = 'SQL' AND anno = 1992) b
        USING (dni)
    ) selected
    USING(dni);

-- 5.1 -  art�culos de 1993 de un estudiante en la universidad de madrid
SELECT titulo_art
  FROM 
    articulo a 
    INNER JOIN 
    autor u
    USING (dni)
  WHERE 
    (a.anno = 1993)
    AND 
    (u.universidad LIKE "Universidad%Madrid");