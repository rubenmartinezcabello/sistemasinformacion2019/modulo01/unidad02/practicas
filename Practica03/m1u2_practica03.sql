﻿/*****************************************************************************
*
*   Módulo 1 - Unidad 2 - Práctica 03
*   Autor: Rubén Martínez Cabello
*
*****************************************************************************/
USE `m1u2_practica03`;

/**
01. Visualizar el número de empleados de cada departamento. Usa GROUP BY para 
  agrupar por departamentos.
**/
SELECT d.dnombre, d.dept_no, COUNT(*) AS num_empleados
  FROM emple e INNER JOIN depart d
  ON e.dept_no = d.dept_no
  GROUP BY e.dept_no;
;

/**
02. Visualizar los departamentos con 5 o más empleados. Usa GROUP_BY y HAVING.
**/
SELECT d.dnombre, d.dept_no, COUNT(*) AS num_empleados
  FROM 
    emple e INNER JOIN depart d ON e.dept_no = d.dept_no
  GROUP BY e.dept_no
  HAVING (num_empleados > 5);
;

/**
03. Media de salarios de cada departamento. Usa GROUP_BY y AVG().
**/
SELECT d.dnombre, d.dept_no, COUNT(*) AS num_empleados, AVG(salario) AS salario_medio
  FROM 
    emple e INNER JOIN depart d ON e.dept_no = d.dept_no
  GROUP BY e.dept_no;

/**
04. Visualiza nombre de empleados del departamento ventas.
**/
SELECT e.apellido, e.oficio, d.dnombre
  FROM 
    emple e INNER JOIN depart d ON e.dept_no = d.dept_no
  WHERE d.dnombre = 'VENTAS' AND e.oficio = 'VENDEDOR';
  ;

/**
05. Visualiza Numero de de empleados del departamento ventas.
**/
SELECT COUNT(*), e.oficio, d.dnombre
  FROM 
    emple e INNER JOIN depart d ON e.dept_no = d.dept_no
  WHERE d.dnombre = 'VENTAS' AND e.oficio = 'VENDEDOR';
  ;

/**
06. Visualiza los oficios del departamento de ventas.
**/
SELECT DISTINCT e.oficio, d.dnombre
  FROM 
    emple e INNER JOIN depart d ON e.dept_no = d.dept_no
  WHERE d.dnombre = 'VENTAS';
  ;

/**
07. Numero de empleados de oficio empleado en cada departamento.
**/
SELECT COUNT(*) AS num_empleados, d.dnombre
  FROM 
    emple e INNER JOIN depart d ON e.dept_no = d.dept_no
  WHERE e.oficio = 'EMPLEADO'
  GROUP BY e.dept_no
  ;

/**
08. Muestra el departamento con más empleados
**/
SELECT MAX(e.num_empleado)
  FROM
  (
    SELECT COUNT(*) AS num_empleado
    FROM 
      emple e INNER JOIN depart d ON e.dept_no = d.dept_no
    WHERE e.oficio = 'EMPLEADO'
    GROUP BY e.dept_no
  ) e;

SELECT COUNT(*) AS num_empleados, d.dnombre
  FROM 
    emple e INNER JOIN depart d ON e.dept_no = d.dept_no
  WHERE e.oficio = 'EMPLEADO'
  GROUP BY e.dept_no
  HAVING num_empleados = (
    SELECT MAX(e.num_empleado)
      FROM
      (
        SELECT COUNT(*) AS num_empleado
        FROM 
          emple e INNER JOIN depart d ON e.dept_no = d.dept_no
        WHERE e.oficio = 'EMPLEADO'
        GROUP BY e.dept_no
      ) e
  );

/**
09. Mostrar los departamentos cuya suma de salarios sea mayor que la media de salarios de todos los empleados. 
**/
SELECT AVG(e.salario) AS salariomediodept, d.dnombre
  FROM 
    emple e INNER JOIN depart d ON e.dept_no = d.dept_no
  GROUP BY e.dept_no;

SELECT AVG(e.salario) AS salariomedio
  FROM 
    emple e;

SELECT AVG(e.salario) AS salariomediodept, d.dnombre
  FROM 
    emple e INNER JOIN depart d ON e.dept_no = d.dept_no
  GROUP BY e.dept_no
  HAVING salariomediodept>(SELECT AVG(e.salario) AS salariomedio FROM emple e);


/**
10. Para cada oficio, suma los salarios
**/
SELECT e.oficio, SUM(e.salario) AS total
  FROM 
    emple e 
  GROUP BY e.oficio
;

/**
11. Suma los sueldos de cada oficio en el departamento de ventas
**/
SELECT e.oficio, COUNT(*) AS num_empleados, SUM(e.salario) AS total
  FROM 
    emple e INNER JOIN depart d ON e.dept_no = d.dept_no
  WHERE d.dnombre = 'VENTAS'
  GROUP BY e.oficio
;

/**
12. Número de departamento que tenga más empleados con oficio empleado
**/
SELECT e.dept_no, COUNT(*) AS num_empleados
  FROM emple e 
  WHERE e.oficio = 'EMPLEADO'
  GROUP BY e.dept_no;

SELECT MAX(numEmpleadosPorDept.num_empleados) AS maximo
  FROM 
  (
    SELECT e.dept_no, COUNT(*) AS num_empleados
      FROM emple e 
      WHERE e.oficio = 'EMPLEADO'
      GROUP BY e.dept_no
  ) numEmpleadosPorDept;


SELECT numEmpleadosPorDept.dept_no
  FROM 
    (
      SELECT MAX(numEmpleadosPorDept.num_empleados) AS maximo
      FROM
      (
        SELECT e.dept_no, COUNT(*) AS num_empleados
          FROM emple e 
          WHERE e.oficio = 'EMPLEADO'
          GROUP BY e.dept_no
      ) numEmpleadosPorDept
    ) maxNumEmpleadosPorDept
    INNER JOIN
    (
      SELECT e.dept_no, COUNT(*) AS num_empleados
        FROM emple e 
        WHERE e.oficio = 'EMPLEADO'
        GROUP BY e.dept_no
    ) numEmpleadosPorDept
    ON maxNumEmpleadosPorDept.maximo = numEmpleadosPorDept.num_empleados;


SELECT e.dept_no
  FROM emple e 
  WHERE e.oficio = 'EMPLEADO'
  GROUP BY e.dept_no
  HAVING COUNT(*) = (
    SELECT MAX(numEmpleadosPorDept.num_empleados) AS maximo
      FROM
      (
        SELECT e.dept_no, COUNT(*) AS num_empleados
          FROM emple e 
          WHERE e.oficio = 'EMPLEADO'
          GROUP BY e.dept_no
      ) numEmpleadosPorDept
    );
  
/**
13. Visualizar el numero de oficios distintos en cada departamento
**/
SELECT d.dnombre, COUNT(DISTINCT e.oficio) AS num_oficios
  FROM 
    emple e INNER JOIN depart d ON e.dept_no = d.dept_no
  GROUP BY d.dnombre
;

/**
14. Mostrar los departamentos que tengan más de 2 personas en la misma profesion
**/
SELECT d.dnombre, e.oficio, COUNT(*) AS num_empleados
  FROM emple e INNER JOIN depart d ON e.dept_no = d.dept_no
  GROUP BY d.dnombre, e.oficio
  HAVING num_empleados >2;

/**
15. Dada la tabla herramientas, muestra en cada estantería la suma de unidades.
**/
SELECT h.estanteria, SUM(h.unidades) AS total_unidades
  FROM herramientas h
  GROUP BY h.estanteria;

/**
16. Visualiza la estanteria con más unidades (con totales y sin totales)
**/

/* Con totales */
SELECT MAX(x.total_unidades) AS max_unidades
  FROM
  (
    SELECT h.estanteria, SUM(h.unidades) AS total_unidades
      FROM herramientas h
      GROUP BY h.estanteria
  ) x;


SELECT h.estanteria, SUM(h.unidades) AS total_unidades
  FROM herramientas h
  GROUP BY h.estanteria
  HAVING total_unidades = (
    SELECT MAX(x.total_unidades) AS max_unidades
    FROM
    (
      SELECT h.estanteria, SUM(h.unidades) AS total_unidades
        FROM herramientas h
        GROUP BY h.estanteria
    ) x
  );

/* Sin totales */
SELECT h.estanteria, SUM(h.unidades) AS total_unidades
  FROM herramientas h
  GROUP BY h.estanteria
  ORDER BY total_unidades DESC
  LIMIT 1;



/**
17. Muestra número de medicos de cada hospital, ordenado por numero descendiente del hospital.
**/
SELECT h.cod_hospital, h.nombre, COUNT(*) AS num_medicos
FROM medicos m INNER JOIN hospitales h ON m.cod_hospital = h.cod_hospital
GROUP BY m.cod_hospital
ORDER BY h.cod_hospital DESC;

/**
18. Muestra de cada hospital, las especialidades que tiene
**/
SELECT DISTINCT h.nombre, m.especialidad
FROM medicos m INNER JOIN hospitales h ON m.cod_hospital = h.cod_hospital
ORDER BY h.cod_hospital;

/**
19. Muestra de cada hospital, las especialidades que tiene
**/
SELECT h.nombre, m.especialidad, COUNT(*) AS num_medicos
FROM medicos m INNER JOIN hospitales h ON m.cod_hospital = h.cod_hospital
GROUP BY m.cod_hospital, m.especialidad
ORDER BY h.cod_hospital;

/**
20. Muestra de cada hospital el numero de empleados
**/
SELECT h.nombre, COUNT(*) AS num_empleados
FROM medicos m INNER JOIN hospitales h ON m.cod_hospital = h.cod_hospital
GROUP BY m.cod_hospital;

/**
21. Muestra de cada especialidad el numero de doctores
**/
SELECT m.especialidad, COUNT(*) AS num_especialistas
FROM medicos m
GROUP BY m.especialidad;


/**
22. Especialidad con más médicos
**/
SELECT MAX(num_especialistas)
  FROM 
  (
    SELECT m.especialidad, COUNT(*) AS num_especialistas
      FROM medicos m
      GROUP BY m.especialidad
  ) max_especialistas;

SELECT m.especialidad, COUNT(*) AS num_especialistas
  FROM medicos m
  GROUP BY m.especialidad
  HAVING num_especialistas = (
    SELECT MAX(num_especialistas)
      FROM 
      (
        SELECT m.especialidad, COUNT(*) AS num_especialistas
          FROM medicos m
          GROUP BY m.especialidad
      ) max_especialistas
  );


/**
23. Hospital con más plazas
**/
SELECT MAX(num_plazas)
  FROM hospitales h;

SELECT h.nombre 
  FROM hospitales h 
  WHERE h.num_plazas = (
    SELECT MAX(num_plazas)
      FROM hospitales h
    );

/**
24. Mostrar las estanterias de herramientas, en orden descendiente
**/
SELECT * 
  FROM herramientas h
  ORDER BY h.estanteria DESC
;

/**
25. Mostrar total de unidades por estanteria
**/
SELECT h.estanteria, SUM(h.unidades) AS total_unidades
  FROM herramientas h
  GROUP BY h.estanteria
;

/**
26. Mostrar estanterias con >15 unidades
**/
SELECT unidades_totales.estanteria
  FROM 
  (
    SELECT h.estanteria, SUM(h.unidades) AS total_unidades
      FROM herramientas h
      GROUP BY h.estanteria
  ) unidades_totales
  WHERE unidades_totales.total_unidades >15
;

/**
27. Estanteria con más unidades
**/
SELECT h.estanteria
  FROM herramientas h
  GROUP BY h.estanteria
  HAVING SUM(h.unidades) = (
    SELECT MAX(unidades_totales.total_unidades)
      FROM 
      (
        SELECT h.estanteria, SUM(h.unidades) AS total_unidades
          FROM herramientas h
          GROUP BY h.estanteria
      ) unidades_totales
  )
;

/**
28. Datos del departamento sin empleados
**/
SELECT DISTINCT dept_no FROM emple;

SELECT * -- Forma 1 con NOT IN
  FROM depart d 
WHERE d.dept_no NOT IN (
  SELECT DISTINCT dept_no 
    FROM emple
  );

SELECT DISTINCT d.* -- Forma 2 con LEFT JOIN
  FROM depart d LEFT OUTER JOIN emple e ON d.dept_no = e.dept_no
  WHERE e.emp_no IS NULL;


/**
29. Suma de de empleados de cada departamento
**/
SELECT e.dept_no,  COUNT(*) AS num_empleados FROM emple e GROUP BY e.dept_no;

SELECT d.*, tot_emp.num_empleados
  FROM depart d LEFT OUTER JOIN (SELECT e.dept_no,  COUNT(*) AS num_empleados FROM emple e GROUP BY e.dept_no) tot_emp ON d.dept_no = tot_emp.dept_no
;


/**
30. Obtener la suma de salarios de cada departamento, mostrando dept_no, suma_de_salarios, dnombre, mostrando los que no tengan empleados
**/

SELECT dept_no, SUM(e.salario) AS suma_de_salarios FROM emple e GROUP BY e.dept_no;
  
SELECT d.*, tot_salarios.suma_de_salarios
  FROM depart d LEFT OUTER JOIN (SELECT dept_no, SUM(e.salario) AS suma_de_salarios FROM emple e GROUP BY e.dept_no) tot_salarios ON d.dept_no = tot_salarios.dept_no
;

/**
31. Usa IFNULL() para que la suma de salarios figure como 0 en lugar de NULL
**/
SELECT d.*, IFNULL(tot_salarios.suma_de_salarios, 0) suma_de_salarios
  FROM depart d LEFT OUTER JOIN (SELECT dept_no, SUM(e.salario) AS suma_de_salarios FROM emple e GROUP BY e.dept_no) tot_salarios ON d.dept_no = tot_salarios.dept_no
;

/**
32. Numero de medicos de cada hospital, incluso los que no tienen médico.
**/
SELECT m.cod_hospital, COUNT(*) AS num_medicos
  FROM medicos m
  GROUP BY m.cod_hospital
;

SELECT h.cod_hospital,
       h.nombre,
       IFNULL(tmedicos.num_medicos,0) num_medicos
  FROM 
    hospitales h 
    LEFT OUTER JOIN 
    (SELECT m.cod_hospital, COUNT(*) AS num_medicos FROM medicos m GROUP BY m.cod_hospital) tmedicos
    ON h.cod_hospital=tmedicos.cod_hospital
;
  