/*****************************************************************************
*
*   M�dulo 1 - Unidad 2 - Pr�ctica 05
*   Autor: Rub�n Mart�nez Cabello
*
*****************************************************************************/
CREATE DATABASE IF NOT EXISTS `m1u2_practica05` 
  CHARACTER SET=utf8mb4 
  COLLATE=utf8mb4_spanish_ci;

USE `m1u2_practica05`;



-- Realizar el script de creaci�n de las tablas y de las restricciones. 
/*
CREATE OR REPLACE TABLE federacion (
  nombre varchar(50),
  direccion varchar(127),
  telefono int(9) UNSIGNED ZEROFILL,
  PRIMARY KEY (nombre)
) ENGINE=INNODB;
  
CREATE OR REPLACE TABLE miembro (
  dni char(9),
  nombre_m varchar (80),
  titulacion varchar(20),
  PRIMARY KEY(dni)
  ) ENGINE=INNODB ;

CREATE OR REPLACE TABLE composicion (
  nombre varchar(50),
  dni char(9),
  cargo varchar(31), 
  fecha_inicio date,
  PRIMARY KEY (nombre, dni)
  ) ENGINE=INNODB ;
*/

-- Restricciones
/*
ALTER TABLE composicion
  ADD CONSTRAINT fk_nombreFederacion
    FOREIGN KEY (nombre) 
      REFERENCES federacion(nombre);

ALTER TABLE composicion
  ADD CONSTRAINT fk_dniMiembro
    FOREIGN KEY (dni) 
      REFERENCES miembro(dni);

ALTER TABLE composicion
  ADD CONSTRAINT uk_cargoFederacion
    UNIQUE KEY (nombre, cargo);
*/


-- Introducir datos en las tablas. 
-- Tools > New Data Generation
-- Ejecutar m1u2_practica05(localhost)_fixed.sql
-- Total 120:  SELECT COUNT(*) FROM composicion;
-- 30 de cada: SELECT cargo, COUNT(*) FROM composicion GROUP BY cargo;

/*
 * Consultas:
 */

-- 1.1 - Nombres de los presidentes (NO OPTIMIZADA)
SELECT nombre_m 
  FROM 
    composicion 
  INNER JOIN 
    miembro
  ON composicion.dni = miembro.dni
  WHERE 
    cargo = 'PRESIDENTE';

-- 1.2 - Nombres de los presidentes (SEMI OPTIMIZADA, refleja el �lgebra solicitada)
SELECT nombre_m 
  FROM 
    composicion 
  INNER JOIN 
    (SELECT dni, nombre_m FROM miembro) miembro
  ON composicion.dni = miembro.dni
  WHERE 
    cargo = 'PRESIDENTE';

-- 1.3 - Nombres de los presidentes (OPTIMIZADA completa)
SELECT nombre_m 
  FROM 
    (SELECT dni, cargo FROM composicion WHERE cargo = 'PRESIDENTE') composicion
  INNER JOIN 
    (SELECT dni, nombre_m FROM miembro) miembro
  ON composicion.dni = miembro.dni;





-- 2.1 - Direcciones de las federaciones con gerentes
SELECT 
* -- federacion.direccion 
  FROM 
    federacion 
  INNER JOIN 
    composicion 
  USING (nombre) 
  WHERE 
    cargo='GERENTE';

-- 2.2 - Direcciones de las federaciones con gerentes (Optimizada)
SELECT 
  federacion.direccion 
  FROM 
    federacion 
  INNER JOIN 
    (SELECT nombre FROM composicion WHERE cargo='GERENTE') composicion
  USING (nombre);




-- 3.1 - Nombres de las distintas federaciones que NO tienen asesores t�cnicos
SELECT nombre FROM federacion;
SELECT DISTINCT nombre FROM composicion WHERE cargo='ASESOR TECNICO';

SELECT nombre FROM federacion 
MINUS 
SELECT DISTINCT nombre FROM composicion WHERE cargo='ASESOR TECNICO';

-- 3.2 - Variante con NOT IN ()
SELECT nombre 
  FROM 
    federacion 
  WHERE nombre NOT IN (SELECT DISTINCT nombre FROM composicion WHERE cargo='ASESOR TECNICO');

-- 3.3 - Variante con LEFT JOIN
SELECT nombre 
  FROM 
    federacion f 
  LEFT JOIN 
    (SELECT DISTINCT nombre FROM composicion WHERE cargo='ASESOR TECNICO') a 
  ON f.nombre = a.nombre 
  WHERE 
    a.nombre IS NULL;




-- 4.1 - Nombre de las federaciones que tienen todos los cargos (DIVISION)
-- Dividendo
SELECT DISTINCT c.nombre, c.cargo FROM composicion c;
-- Divisor
SELECT DISTINCT c.cargo FROM composicion c;
SELECT COUNT(DISTINCT c.cargo) FROM composicion c;

-- 4.1 -
SELECT DISTINCT nombre, cargo FROM composicion
DIV
SELECT DISTINCT cargo FROM composicion;
-- �
SELECT DISTINCT nombre, cargo FROM composicion
DIV
SELECT DISTINCT cargo FROM composicion;


-- 4.2 - Simular division mediante totales. Hacer una combinacion de elementos - relaciones 

SELECT personas.nombre, COUNT(*)
  FROM 
    (SELECT DISTINCT nombre, cargo FROM composicion) personas   
    GROUP BY personas.nombre
    HAVING COUNT(*) = (SELECT COUNT(DISTINCT c.cargo) FROM composicion c);
;

/** https://www.youtube.com/watch?v=5jbqsiKsHMU
  This video explains a way to answer the "division" question in relational databases.  
  The division problem asks: Which records in data set A correspond to every record in data set B?  
  For example, which employees have attended all the company's training sessions?  
  The problem is answered by using a subquery in the HAVING clause.
**/



-- 5.1 - Nombres de las federaciones que tienen asesor t�cnico y psicologo (Interseccion)
SELECT c.nombre FROM composicion c WHERE c.cargo = 'ASESOR TECNICO';
SELECT c.nombre FROM composicion c WHERE c.cargo = 'PSICOLOGO';

SELECT c.nombre FROM composicion c WHERE c.cargo = 'ASESOR TECNICO'
INTERSECT
SELECT c.nombre FROM composicion c WHERE c.cargo = 'PSICOLOGO';

-- 5.2 - Nombres de las federaciones que tienen asesor t�cnico y psicologo (Inner join)
SELECT c1.nombre
  FROM composicion c1 INNER JOIN composicion c2 USING (nombre)
  WHERE c1.cargo = 'ASESOR TECNICO' AND c2.cargo = 'PSICOLOGO';


/*
DROP TABLE composicion;
DROP TABLE miembro;
DROP TABLE federacion;
*/