﻿/*****************************************************************************
*
*   Módulo 1 - Unidad 2 - Práctica 04
*   Autor: Rubén Martínez Cabello
*
*****************************************************************************/
USE `m1u2_practica04`;

/**
01. Averigua el DNI de todos los clientes. 
**/
SELECT dni FROM cliente c;

/**
02. Consulta todos los datos de todos los programas. 
**/
SELECT * FROM programa p;

/**
03. Obtén un listado con los nombres de todos los programas. 
**/
SELECT DISTINCT p.nombre FROM programa p;

/**
04. Genera una lista con todos los comercios. 
**/
SELECT * FROM comercio c;

/**
05. Genera una lista de las ciudades con establecimientos donde se venden programas, sin que aparezcan valores duplicados (utiliza DISTINCT). 
**/
SELECT DISTINCT c.ciudad FROM comercio c;

/**
06. Obtén una lista con los nombres de programas, sin que aparezcan valores duplicados (utiliza DISTINCT). 
**/
SELECT DISTINCT p.nombre FROM programa p;

/**
07. Obtén el DNI más 4 de todos los clientes. 
**/
SELECT (c.dni + 4) FROM cliente c;

/**
08. Haz un listado con los códigos de los programas multiplicados por 7. 
**/
SELECT codigo*7 FROM programa p;

/**
09. ¿Cuáles son los programas cuyo código es inferior o igual a 10? 
**/
SELECT * FROM programa p WHERE p.codigo <= 10;

/**
10. ¿Cuál es el programa cuyo código es 11? 
**/
SELECT * FROM programa p WHERE p.codigo = 11;

/**
11. ¿Qué fabricantes son de Estados Unidos? 
**/
SELECT f.nombre FROM fabricante f WHERE pais = 'Estados Unidos';

/**
12. ¿Cuáles son los fabricantes no españoles? Utilizar el operador IN. 
**/
SELECT f.nombre FROM fabricante f WHERE pais NOT IN ('España');

/**
13. Obtén un listado con los códigos de las distintas versiones de Windows. 
**/
SELECT p.version FROM programa p WHERE p.nombre = 'Windows';

/**
14. ¿En qué ciudades comercializa programas El Corte Inglés? 
**/
SELECT DISTINCT c.ciudad FROM comercio c WHERE c.nombre = 'El Corte Inglés';

/**
15. ¿Qué otros comercios hay, además de El Corte Inglés? Utilizar el operador IN. 
**/
SELECT DISTINCT c.nombre FROM comercio c WHERE c.nombre NOT IN ('El Corte Inglés');

/**
16. Genera una lista con los códigos de las distintas versiones de Windows y Access. Utilizar el operador IN. 
**/
SELECT DISTINCT p.version FROM programa p WHERE p.nombre IN ('Windows', 'Access');

/**
17. Obtén un listado que incluya los nombres de los clientes de edades comprendidas entre 10 y 25 y de los mayores de 50 años. Da una solución con BETWEEN y otra sin BETWEEN. 
**/
SELECT c.nombre FROM cliente c WHERE c.edad BETWEEN 10 AND 25 OR (c.edad>50);
SELECT c.nombre FROM cliente c WHERE (c.edad >=18) AND (c.edad <=25) OR (c.edad>50);

/**
18. Saca un listado con los comercios de Sevilla y Madrid. No se admiten valores duplicados. 
**/
SELECT DISTINCT c.nombre FROM comercio c WHERE c.ciudad IN ('Sevilla','Madrid');

/**
19. ¿Qué clientes terminan su nombre en la letra “o”? 
**/
SELECT DISTINCT c.nombre FROM cliente c WHERE c.nombre LIKE '%o';

/**
20. ¿Qué clientes terminan su nombre en la letra “o” y, además, son mayores de 30 años? 
**/
SELECT DISTINCT c.nombre FROM cliente c WHERE c.nombre LIKE '%o' AND c.edad>30;

/**
21. Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A o por una W. 
**/
SELECT * FROM programa p WHERE p.version LIKE '%i' OR p.nombre LIKE 'a%' OR p.nombre LIKE 'w%';

/**
22. Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, o cuyo nombre comience por una A y termine por una S.
**/
SELECT * FROM programa p WHERE p.version LIKE '%i' OR p.nombre LIKE 'a%s';

/**
23. Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, y cuyo nombre no comience por una A. 
**/
SELECT * FROM programa p WHERE p.version LIKE '%i' AND p.nombre NOT LIKE 'a%';

/**
24. Obtén una lista de empresas por orden alfabético ascendente. 
**/
SELECT DISTINCT * FROM fabricante f ORDER BY f.nombre ASC;

/**
25. Genera un listado de empresas por orden alfabético descendente. 
**/
SELECT DISTINCT * FROM fabricante f ORDER BY f.nombre DESC;

/**
26. Obtén un listado de programas por orden de versión 
**/
SELECT * FROM programa p ORDER BY version ASC;

/**
27. Genera un listado de los programas que desarrolla Oracle. 
**/
SELECT DISTINCT p.nombre producto, f.nombre fabricante 
  FROM programa p INNER JOIN desarrolla d INNER JOIN fabricante f 
  ON p.codigo=d.codigo AND d.id_fab = f.id_fab WHERE f.nombre = 'Oracle';

/**
28. ¿Qué comercios distribuyen Windows? 
**/
SELECT DISTINCT c.nombre, c.ciudad FROM comercio c INNER JOIN distribuye d INNER JOIN programa p ON c.cif = d.cif AND d.codigo = p.codigo WHERE p.nombre = 'Windows';

/**
29. Genera un listado de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid. 
**/
SELECT p.nombre, p.version, d.cantidad FROM comercio c INNER JOIN distribuye d INNER JOIN programa p ON c.cif = d.cif AND d.codigo = p.codigo WHERE c.nombre = 'EL CORTE INGLES' AND ciudad = 'MADRID';

/**
30. ¿Qué fabricante ha desarrollado Freddy Hardest? 
**/
SELECT f.nombre FROM programa p INNER JOIN desarrolla d INNER JOIN fabricante f ON p.codigo=d.codigo AND d.id_fab = f.id_fab WHERE p.nombre = 'Freddy Hardest';

/**
31. Selecciona el nombre de los programas que se registran por Internet. 
**/
SELECT DISTINCT p.nombre, p.version FROM registra r INNER JOIN programa p USING (codigo) WHERE medio='internet';

/**
32. Selecciona el nombre de las personas que se registran por Internet. 
**/
SELECT DISTINCT c.nombre FROM registra r INNER JOIN cliente c ON r.dni = c.dni WHERE medio='internet';

/**
33. ¿Qué medios ha utilizado para registrarse Pepe Pérez? 
**/
SELECT DISTINCT r.medio FROM registra r INNER JOIN cliente c ON r.dni = c.dni WHERE c.nombre='PEPE PEREZ';

/** ???
34. ¿Qué usuarios han optado por Internet como medio de registro? 
**/
SELECT DISTINCT c.nombre FROM registra r INNER JOIN cliente c ON r.dni = c.dni WHERE medio='internet';


/**
35. ¿Qué programas han recibido registros por tarjeta postal? 
**/
SELECT DISTINCT p.nombre, p.version FROM registra r INNER JOIN programa p USING (codigo) WHERE medio='tarjeta postal';

/**
36. ¿En qué localidades se han vendido productos que se han registrado por Internet? 
**/
SELECT c.ciudad
  FROM registra r INNER JOIN programa p INNER JOIN distribuye d INNER JOIN comercio c
  ON r.codigo = p.codigo AND p.codigo = d.codigo AND d.cif = c.cif
  WHERE medio = 'internet';
/**
37. Obtén un listado de los nombres de las personas que se han registrado por Internet, junto al nombre de los programas para los que ha efectuado el registro. 
**/
SELECT c.nombre , p.nombre AS programa, p.version
  FROM registra r INNER JOIN programa p INNER JOIN cliente c
  ON r.codigo = p.codigo AND r.dni = c.dni
  WHERE medio = 'internet';


/**
38. Genera un listado en el que aparezca cada cliente junto al programa que ha registrado, el medio con el que lo ha hecho y el comercio en el que lo ha adquirido. 
**/
SELECT c.nombre, p.nombre, p.version, r.medio, s.nombre comercio
  FROM registra r INNER JOIN programa p INNER JOIN cliente c INNER JOIN comercio s
  ON r.codigo = p.codigo AND r.dni = c.dni AND r.cif = s.cif
;

/**
39. Genera un listado con las ciudades en las que se pueden obtener los productos de Oracle. 
**/
SELECT DISTINCT c.ciudad
  FROM fabricante f INNER JOIN desarrolla des INNER JOIN programa p INNER JOIN distribuye dis INNER JOIN comercio c
  ON (f.id_fab = des.id_fab) AND (des.codigo = p.codigo) AND (p.codigo = dis.codigo) AND (dis.cif = c.cif)
  WHERE f.nombre = 'Oracle'
;

/**
40. Obtén el nombre de los usuarios que han registrado Access XP. 
**/
SELECT c.nombre , p.nombre AS programa, p.version
  FROM registra r INNER JOIN programa p INNER JOIN cliente c
  ON r.codigo = p.codigo AND r.dni = c.dni
  WHERE p.nombre = 'Access' and p.version = 'XP';


/**
41. Nombre de aquellos fabricantes cuyo país es el mismo que ʻOracleʼ. (Subconsulta). 
**/
SELECT pais FROM fabricante f WHERE f.nombre = 'ORACLE';
SELECT nombre FROM fabricante f WHERE f.pais = (SELECT pais FROM fabricante f WHERE f.nombre = 'ORACLE');

/**
42. Nombre de aquellos clientes que tienen la misma edad que Pepe Pérez. (Subconsulta). 
**/
SELECT c.edad FROM cliente c WHERE c.nombre = 'Pepe Pérez';
SELECT c.nombre FROM cliente c WHERE c.edad = (SELECT c.edad FROM cliente c WHERE c.nombre = 'Pepe Pérez');

/**
43. Genera un listado con los comercios que tienen su sede en la misma ciudad que tiene el comercio ʻFNACʼ. (Subconsulta). 
**/
SELECT ciudad FROM comercio c WHERE c.nombre = 'FNAC';
SELECT c.nombre FROM comercio c WHERE c.ciudad = (SELECT ciudad FROM comercio c WHERE c.nombre = 'FNAC');

/**
44. Nombre de aquellos clientes que han registrado un producto de la misma forma que el cliente ʻPepe Pérezʼ. (Subconsulta). 
**/
SELECT r.medio FROM registra r INNER JOIN cliente c ON r.dni = c.dni WHERE c.nombre = 'Pepe Pérez';
SELECT DISTINCT c.nombre FROM registra r INNER JOIN cliente c ON r.dni = c.dni WHERE r.medio IN (SELECT r.medio FROM registra r INNER JOIN cliente c ON r.dni = c.dni WHERE c.nombre = 'Pepe Pérez');

/**
45. Obtener el número de programas que hay en la tabla programas. 
**/
SELECT COUNT(*) FROM programa p;
/**
46. Calcula el número de clientes cuya edad es mayor de 40 años. 
**/
SELECT COUNT(*) FROM cliente c WHERE c.edad > 40;

/**
47. Calcula el número de productos que ha vendido el establecimiento cuyo CIF es 1. 
**/
SELECT COUNT(*) FROM distribuye d WHERE d.cif = 1;

/**
48. Calcula la media de programas que se venden cuyo código es 7. 
**/
SELECT AVG(d.cantidad) FROM distribuye d WHERE d.codigo = 7;

/**
49 Calcula la mínima cantidad de programas de código 7 que se ha vendido 
**/
SELECT MIN(d.cantidad) FROM distribuye d WHERE d.codigo = 7;

/**
50 Calcula la máxima cantidad de programas de código 7 que se ha vendido. 
**/
SELECT MAX(d.cantidad) FROM distribuye d WHERE d.codigo = 7;

/**
51 ¿En cuántos establecimientos se vende el programa cuyo código es 7? 
**/
SELECT COUNT(*) FROM distribuye d WHERE d.codigo = 7;

/**
52 Calcular el número de registros que se han realizado por Internet. 
**/
SELECT COUNT(*) FROM registra r WHERE r.medio = 'internet';

/**
53 Obtener el número total de programas que se han vendido en ʻSevillaʼ. 
**/
SELECT SUM(cantidad) FROM distribuye d INNER JOIN comercio c USING(cif) WHERE c.ciudad = 'SEVILLA';


/**
54 Calcular el número total de programas que han desarrollado los fabricantes cuyo país es ʻEstados Unidosʼ. 
**/
SELECT COUNT(d.codigo) FROM desarrolla d INNER JOIN fabricante f USING (id_fab) WHERE f.pais = 'Estados Unidos';

/**
55 Visualiza el nombre de todos los clientes en mayúscula. En el resultado de la consulta debe aparecer también la longitud de la cadena nombre. 
**/
SELECT UPPER(c.nombre) nombre, CHAR_LENGTH(c.nombre) longitud FROM cliente c;

/**
56 Con una consulta concatena los campos nombre y versión de la tabla PROGRAMA. 
**/
SELECT p.codigo , TRIM(concat(p.nombre, ' ',  IFNULL(p.version, ''))) nombre FROM  programa p;
