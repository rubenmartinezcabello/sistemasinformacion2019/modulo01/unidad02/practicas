USE reservas;

-- 1 Importa base de datos

-- 2 Foreign keys 

ALTER TABLE TIPO_HABITACION
  DROP CONSTRAINT IF EXISTS fk_hotel_habitacion;
ALTER TABLE RESERVA
  DROP CONSTRAINT IF EXISTS fk_tipohab_reserva;
ALTER TABLE RESERVA
  DROP CONSTRAINT IF EXISTS fk_numcli_cliente;


ALTER TABLE TIPO_HABITACION
  ADD CONSTRAINT fk_hotel_habitacion 
    FOREIGN KEY (NUM_HOTEL) REFERENCES HOTEL (NUM_HOTEL);
ALTER TABLE RESERVA
  ADD CONSTRAINT fk_tipohab_reserva
    FOREIGN KEY (NUM_TIPOHAB) REFERENCES TIPO_HABITACION (NUM_TIPOHAB);
ALTER TABLE RESERVA
  ADD CONSTRAINT fk_numcli_cliente
    FOREIGN KEY (NUM_CLIENTE) REFERENCES CLIENTE (NUM_CLIENTE);


-- 3 Esquema de relaciones : DiagramaRelaciones.dbd

-- 4.1 N�mero de habitaciones por cada hotel (codigo y n�mero)
SELECT NUM_HOTEL, COUNT(*) NUM_HABITACIONES 
  FROM tipo_habitacion th 
  GROUP BY th.NUM_HOTEL;

-- 4.2 N�mero de habitaciones por cada hotel (Nombre, direccion y n�mero)
SELECT h.NOMBRE, h.DOMICILIO, COUNT(*) NUM_HABITACIONES
  FROM hotel h INNER JOIN tipo_habitacion th ON h.NUM_HOTEL = th.NUM_HOTEL 
  GROUP BY th.NUM_HOTEL;

-- 4.3 Nombre del hotel con mayor n�mero de habitaciones
SELECT COUNT(*) c
  FROM tipo_habitacion th
  GROUP BY th.NUM_HOTEL;

SELECT MAX(th.c) 
  FROM (
    SELECT COUNT(*) c
      FROM tipo_habitacion th
      GROUP BY th.NUM_HOTEL
  ) th;


SELECT h.NOMBRE, COUNT(*) c
  FROM hotel h INNER JOIN tipo_habitacion th ON h.NUM_HOTEL = th.NUM_HOTEL
  GROUP BY th.NUM_HOTEL
  HAVING COUNT(*) = (
    SELECT MAX(th.c) 
      FROM (
        SELECT COUNT(*) c
          FROM tipo_habitacion th
          GROUP BY th.NUM_HOTEL
      ) th
    );

-- 4.4 Hoteles con el segundo numero de habitaciones
  SELECT DISTINCT COUNT(*) c
  FROM tipo_habitacion th
  GROUP BY th.NUM_HOTEL
  ORDER BY c DESC;

SET @row_count=0;
SELECT (@row_count := @row_count+1) puesto, c
  FROM (
    SELECT DISTINCT COUNT(*) c
      FROM tipo_habitacion th
      GROUP BY th.NUM_HOTEL
      ORDER BY c DESC
  ) origin
GROUP BY c;

SET @row_count=0;
SELECT origin.c 
  FROM (
    SELECT (@row_count := @row_count+1) puesto, c
      FROM (
        SELECT DISTINCT COUNT(*) c
          FROM tipo_habitacion th
          GROUP BY th.NUM_HOTEL
          ORDER BY c DESC
      ) origin
      GROUP BY c
  ) origin
  WHERE puesto = 2
;

SET @row_count=0;
SELECT h.NOMBRE, COUNT(*) c
  FROM hotel h INNER JOIN tipo_habitacion th ON h.NUM_HOTEL = th.NUM_HOTEL
  GROUP BY th.NUM_HOTEL
  HAVING COUNT(*) = (SELECT origin.c 
  FROM (
    SELECT (@row_count := @row_count+1) puesto, c
      FROM (
        SELECT DISTINCT COUNT(*) c
          FROM tipo_habitacion th
          GROUP BY th.NUM_HOTEL
          ORDER BY c DESC
      ) origin
      GROUP BY c
  ) origin
  WHERE puesto = 2);

-- 4.5 Indicar el nombre de los hoteles que todavia no tenemos habitaciones introducidas 
SELECT h.NOMBRE
  FROM 
    hotel h LEFT JOIN tipo_habitacion th 
    ON h.NUM_HOTEL = th.NUM_HOTEL
  GROUP BY th.NUM_HOTEL
  HAVING COUNT(th.NUM_HOTEL) = 0
;

-- 4.6 Indicar el nombre del hotel del cual no se han reservado nunca habitaciones (teniendo en cuenta solamente los hoteles de los cuales hemos introducido sus habitaciones). 
SELECT * FROM hotel h;
SELECT * FROM reserva r ;


SELECT h.nombre hotel, th.NUM_HOTEL, th.NUM_TIPOHAB, COUNT(*) NUM_RESERVAS
  FROM 
      hotel h 
    INNER JOIN tipo_habitacion th ON h.NUM_HOTEL = th.NUM_HOTEL  -- Inner para solo los que tienen habitaciones, outer left para los que no se han metido aun
    INNER JOIN reserva r ON th.NUM_TIPOHAB = r.NUM_TIPOHAB
  GROUP BY h.NUM_HOTEL
  HAVING NUM_RESERVAS = 0
;

-- 5 Importar datos adicionales

-- 5.7 Indicar el mes que mas reservas han comenzado.
SELECT MONTH(FECHA_INI) mes, COUNT(*) reservas
  FROM reserva r
  GROUP BY mes
;

SELECT MAX(reservas) 
  FROM (
    SELECT MONTH(FECHA_INI) mes, COUNT(*) reservas
      FROM reserva r
      GROUP BY mes
  ) reservas
;

SELECT MONTH(FECHA_INI) mes
  FROM reserva r
  GROUP BY mes
  HAVING COUNT(*) = (
    SELECT MAX(reservas) 
      FROM (
        SELECT MONTH(FECHA_INI) mes, COUNT(*) reservas
          FROM reserva r
          GROUP BY mes
      ) reservas
  )
;

-- 5.8 Listar el num_hotel y los distintos meses en que se han comenzado reservas 
SELECT th.NUM_HOTEL , MONTH(r.FECHA_INI) mes
  FROM reserva r INNER JOIN tipo_habitacion th ON r.NUM_TIPOHAB = th.NUM_TIPOHAB
  GROUP BY num_hotel, mes;


-- 5.9 Listar los nombres de los hoteles que han comenzado reservas en todos los meses que han comenzado reservas el hotel numero 4.
SELECT MONTH(r.FECHA_INI) mes
  FROM reserva r INNER JOIN tipo_habitacion th ON r.NUM_TIPOHAB = th.NUM_TIPOHAB
  WHERE th.NUM_HOTEL = 4
  GROUP BY num_hotel, mes;


-- dividendo / divisor = (cociente*divisor) / divisor
-- Divisor
SELECT MONTH(r4.FECHA_INI) mes4
  FROM 
    reserva r4 
    INNER JOIN tipo_habitacion th4 ON r4.NUM_TIPOHAB = th4.NUM_TIPOHAB
  WHERE th4.NUM_HOTEL = 4
  GROUP BY num_hotel, mes4;

-- dividendo
SELECT th.NUM_HOTEL , MONTH(r.FECHA_INI) mes
  FROM 
    reserva r 
    INNER JOIN tipo_habitacion th ON r.NUM_TIPOHAB = th.NUM_TIPOHAB
  GROUP BY num_hotel, mes;


-- divisor*cociente / dividendo
SELECT cociente.NUM_HOTEL
  FROM 
    ( SELECT MONTH(r4.FECHA_INI) mes4 FROM reserva r4 INNER JOIN tipo_habitacion th4 ON r4.NUM_TIPOHAB = th4.NUM_TIPOHAB WHERE th4.NUM_HOTEL = 4 GROUP BY num_hotel, mes4 ) divisor
    INNER JOIN 
    ( SELECT th.NUM_HOTEL , MONTH(r.FECHA_INI) mes FROM  reserva r INNER JOIN tipo_habitacion th ON r.NUM_TIPOHAB = th.NUM_TIPOHAB GROUP BY num_hotel, mes ) cociente
    ON divisor.mes4 = cociente.mes
    GROUP BY cociente.NUM_HOTEL
    HAVING COUNT(*) = (
    SELECT COUNT(*)
      FROM (
        SELECT DISTINCT MONTH(r4.FECHA_INI)
          FROM 
            reserva r4 
            INNER JOIN tipo_habitacion th4 ON r4.NUM_TIPOHAB = th4.NUM_TIPOHAB
          WHERE th4.NUM_HOTEL = 4
      ) meses
    )
;

-- 5.10 Listar los d�as totales que se han reservado cada una de las habitaciones. Indicar en el listado el nombre del hotel al que pertenece.
SELECT r.NUM_TIPOHAB idHab, h.NOMBRE Hotel, DATEDIFF(r.FECHA_FIN, r.FECHA_INI) dias
  FROM reserva r INNER JOIN tipo_habitacion th ON r.NUM_TIPOHAB = th.NUM_TIPOHAB INNER JOIN hotel h ON th.NUM_HOTEL = h.NUM_HOTEL;


SELECT diasReservas.Hotel, diasReservas.idHab, SUM(diasReservas.dias) totalDias
  FROM (
    SELECT r.*, r.NUM_TIPOHAB idHab, h.NOMBRE Hotel, DATEDIFF(r.FECHA_FIN, r.FECHA_INI) dias
      FROM reserva r INNER JOIN tipo_habitacion th ON r.NUM_TIPOHAB = th.NUM_TIPOHAB INNER JOIN hotel h ON th.NUM_HOTEL = h.NUM_HOTEL
  ) diasReservas
  GROUP BY diasReservas.idHab;


-- 5.11 Indicar el nombre de los 2 hoteles que mayor n�mero de habitaciones se han reservado durante el primer trimestre de cualquier a�o.
SELECT th.NUM_HOTEL, COUNT(*) reservas
  FROM 
    reserva r 
    INNER JOIN tipo_habitacion th ON r.NUM_TIPOHAB = th.NUM_TIPOHAB
  WHERE QUARTER(r.FECHA_INI) = 1
  GROUP BY th.NUM_HOTEL
  ORDER BY reservas DESC
;


SELECT h.NOMBRE
  FROM 
  hotel h
  INNER JOIN
  (
    SELECT th.NUM_HOTEL, COUNT(*) reservas
      FROM 
        reserva r 
        INNER JOIN tipo_habitacion th ON r.NUM_TIPOHAB = th.NUM_TIPOHAB
      WHERE QUARTER(r.FECHA_INI) = 1
      GROUP BY th.NUM_HOTEL
      ORDER BY reservas DESC
      LIMIT 0,2
  ) reservas
  ON h.NUM_HOTEL = reservas.NUM_HOTEL;