﻿USE  m1U2_practica01;

/*****************************************************************************
*
*   Módulo 1 - Unidad 2 - Práctica 01
*   Autor: Rubén Martínez Cabello
*
*****************************************************************************/

-- Query 1
SELECT DISTINCT * FROM emple e;

-- Query 2
SELECT DISTINCT * FROM depart d;

-- Query 3
SELECT DISTINCT e.apellido, e.oficio FROM emple e;

-- Query 4
SELECT DISTINCT d.loc, d.dept_no FROM depart d;

-- Query 5
SELECT DISTINCT d.dnombre, d.dept_no, loc FROM depart d;

-- Query 6
SELECT DISTINCT COUNT(*) AS total_empleados FROM emple e;

-- Query 7
SELECT DISTINCT e.apellido FROM emple e ORDER BY e.apellido ASC;

-- Query 8
SELECT DISTINCT e.apellido FROM emple e ORDER BY e.apellido DESC;

-- Query 9
SELECT DISTINCT COUNT(*) AS total_depart FROM depart d;

-- Query 10
-- El UNION ALL es por si hay numeros de empleado que coincidan con numeros de departamento
SELECT COUNT(*) AS total 
  FROM 
    (
     SELECT dept_no FROM depart
     UNION ALL
     SELECT emp_no FROM emple
    ) AS u;

-- Query 11
 SELECT 
    (SELECT COUNT(*) FROM depart)
    +
    (SELECT COUNT(*) FROM emple)
  AS total;

-- Query 12
SELECT * FROM emple ORDER BY dept_no DESC;

-- Query 13
SELECT * FROM emple ORDER BY dept_no DESC, apellido ASC;

-- Query 14
SELECT e.emp_no FROM emple e WHERE e.salario>2000;

-- Query 15
SELECT e.emp_no, e.apellido FROM emple e WHERE e.salario<2000;

-- Query 16
SELECT * FROM emple e WHERE e.salario BETWEEN 1500 AND 2500;

-- Query 17
SELECT * FROM emple e WHERE e.oficio = "ANALISTA";


-- Query 18
SELECT * FROM emple e WHERE oficio = "ANALISTA" AND salario > 2000;

-- Query 19
SELECT e.apellido, e.oficio FROM emple e WHERE dept_no = 20;

-- Query 20
SELECT COUNT(*) AS total FROM emple e WHERE e.oficio = "VENDEDOR";

-- Query 21
SELECT * 
  FROM (
    SELECT * FROM emple e WHERE e.apellido LIKE "m%"
  UNION
    SELECT * FROM emple e WHERE e.apellido LIKE "n%"
  ) AS mn
  ORDER BY mn.apellido ASC;

SELECT * 
  FROM emple e 
  WHERE e.apellido LIKE "m%" OR e.apellido LIKE "n%" 
  ORDER BY e.apellido ASC;

-- Query 22
SELECT * FROM emple e WHERE oficio = "VENDEDOR" ORDER BY apellido ASC;

-- Query 23
SELECT e.apellido 
  FROM emple e
  WHERE e.salario = (SELECT MAX(se.salario) FROM emple se);

-- Query 24
SELECT * 
  FROM ( 
    SELECT * FROM emple e WHERE e.dept_no=10 
    INTERSECT
    SELECT * FROM emple e WHERE e.oficio="ANALISTA"
  ) AS do
  ORDER BY do.apellido ASC, do.oficio ASC;

SELECT * 
  FROM emple e 
  WHERE e.apellido LIKE "m%" OR e.apellido LIKE "n%"
  ORDER BY e.apellido ASC;

-- Query 25
SELECT DISTINCT MONTH(e.fecha_alt) AS mes FROM emple e;

-- Query 26
SELECT DISTINCT YEAR(e.fecha_alt) AS anio FROM emple e;

-- Query 27
SELECT DISTINCT DAY(e.fecha_alt) AS dia FROM emple e;

-- Query 28
SELECT ds.apellido
  FROM (
    SELECT * FROM emple e WHERE e.dept_no = 20
    UNION
    SELECT * FROM emple e WHERE e.salario>2000
  ) AS ds
  ORDER BY ds.apellido ASC;

SELECT e.apellido 
  FROM emple e 
  WHERE  (e.dept_no = 20) OR (e.salario>2000) ORDER BY e.apellido ASC;

-- Query 29
SELECT e.apellido, d.dnombre 
  FROM emple e INNER JOIN depart d
  USING (dept_no);

-- Query 30
SELECT e.apellido, e.oficio, d.dnombre
  FROM 
    emple e 
    INNER JOIN depart d USING (dept_no)
  ORDER BY e.apellido DESC;

-- Query 31
SELECT e.dept_no, COUNT(e.dept_no) AS "NUMERO_DE_EMPLEADOS"
  FROM
    emple e
  GROUP BY e.dept_no
  ORDER BY e.dept_no ASC;

-- Query 32
SELECT d.dnombre AS "DEPARTAMENTO", COUNT(e.dept_no) AS "NUMERO_DE_EMPLEADOS"
  FROM
    emple e 
    INNER JOIN depart d ON e.dept_no = d.dept_no
  GROUP BY e.dept_no
  ORDER BY e.dept_no ASC;

-- Query 33
SELECT e.apellido
  FROM emple e
  ORDER BY e.oficio ASC, e.apellido ASC;

-- Query 34
SELECT e.apellido
  FROM emple e
  WHERE e.apellido LIKE "A%";

-- Query 35
SELECT e.apellido
  FROM (
    SELECT * FROM emple ea WHERE ea.apellido LIKE "A%"
    UNION
    SELECT * FROM emple em WHERE em.apellido LIKE "M%"
  ) AS e;

SELECT e.apellido
  FROM emple e
  WHERE e.apellido LIKE "A%" OR e.apellido LIKE "M%";

-- Query 36
-- ORACLE: MINUS
-- SQL Server: EXCEPT
-- MySQL: unsupported
SELECT * 
  FROM (
    SELECT * FROM emple e
    MINUS
    SELECT * FROM emple ez WHERE ez.apellido LIKE "%Z"
  ) AS difference;

SELECT minuendo.*
FROM
  (SELECT * FROM emple e) minuendo
  LEFT JOIN
  (SELECT * FROM emple ez WHERE ez.apellido LIKE "%Z") sustraendo
  ON minuendo.emp_no = sustraendo.emp_no
  WHERE sustraendo.emp_no IS NULL;

SELECT *
  FROM emple e
  WHERE NOT e.apellido LIKE "%Z";


-- Query 37
SELECT * 
  FROM 
    SELECT * FROM emple e WHERE e.apellido LIKE "%A"
    INTERSECT
    SELECT * FROM emple e WHERE e.oficio LIKE "%E%"
  ;

SELECT *
  FROM emple e
  WHERE e.apellido LIKE "%A" AND e.oficio LIKE "%E%";
